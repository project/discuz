<?php

define('DISCUZ_FORUM_DIR', DISCUZ_MOD_DIR . '/forum');
require_once DISCUZ_FORUM_DIR . '/config.inc.php';


/*
 * Define some Discuz database tables.
 * 
 * */
define('DISCUZ_DB_PREFIX', $tablepre);
define('DISCUZ_MEMBERS_TABLE', DISCUZ_DB_PREFIX . 'members');
define('DISCUZ_POSTS_TABLE', DISCUZ_DB_PREFIX . 'posts');

/*
 * Define private key between Drupal and Discuz.
 * 
 * */
define('PRIVATE_KEY', variable_get('private_key', 'discuz_drupal_module'));

/*
 * Displays the most recent 10 forum subjects.
 * 
 * */
function _discuz_block_new_forum_topics()
{
	global $base_url;
	
	$output = '<ul>';

	$result = db_query("SELECT `tid`, `subject` FROM `" . DISCUZ_POSTS_TABLE 
	.  "` WHERE invisible = '%d' ORDER BY `dateline` DESC LIMIT 0, 10", 0);
	while ($obj = db_fetch_object($result)) {
		$output .= '<li>'. l($obj->subject, 
		$base_url . '/modules/discuz/forum/viewthread.php?tid=' . $obj->tid) 
		. '</li>';
	}
	
	$output .= '</ul>';
	
	return $output;
}

/* 
 * Insert user into Discuz.
 * 
 * Parsed $edit we can get such as Array ( [name] => sirtoozee 
 * [mail] => sirtoozee@localhost.com 
 * [pass] => PWrcfP2V5N [init] => sirtoozee@localhost.com 
 * [roles] => [status] => 1 [uid] => 2 [created] => 1171342964 )  
 * 
 * */
function _discuz_user_insert($edit)
{
	db_query("INSERT INTO `" . DISCUZ_MEMBERS_TABLE 
	. "` (`uid`, `username`, `password`, `regdate`, `email`) 
	VALUES ('%d', '%s', '%s', '%d', '%s')", $edit['uid'], $edit['name'], 
	md5($edit['pass']), $edit['created'], $edit['mail']);
}

/*
 * Update user with Discuz.
 * 
 * Parsed $edit we can ge such as Array ( [timezone] => 28800 
 * [mail] => sirtoozee@localhost.com [pass] => 123456 [signature] => ) 
 * 
 * */
function _discuz_user_update($edit)
{
	global $user;
	
	// Attention that $user->pass has been md5, but $edit['pass'] hasn`t
	db_query("UPDATE `" . DISCUZ_MEMBERS_TABLE 
	. "` SET password = '%s', email = '%s' WHERE uid = '%d'", 
	!empty($edit['pass']) ? md5($edit['pass']) :$user->pass, 
	$edit['mail'], $user->uid);
}

/*
 * Delete user from Discuz.
 * 
 * */
function _discuz_user_delete($uid)
{
	db_query("DELETE FROM `" . DISCUZ_MEMBERS_TABLE . "` WHERE uid = '%d'", $uid);
}

/* 
 * User login via Discuz Passport API.
 * 
 * Parsed $account we can get such as $account->name, $account->pass, 
 * $account->mail
 * 
 * */
function _discuz_user_login($account)
{
	global $base_url;
	
	$action		= 'login';
	$member = array(
		'cookietime'	=> '',
		'time'				=> time(),
		'username'		=> $account->name,
		'password'		=> $account->pass,
		'email'				=> $account->mail,
		'credits'			=> '',
		'regip'				=> '',
		'regdate'			=> '',
		'msn'					=> ''
	);
	$auth				= passport_encrypt(passport_encode($member), PRIVATE_KEY);
	$forward		= $base_url;
	$verify			= md5($action . $auth . $forward . PRIVATE_KEY);

	drupal_goto($base_url . "/modules/discuz/forum/api/passport.php" . 
		"?action=$action" . 
		"&auth=" . rawurlencode($auth) . 
		"&forward=" . rawurlencode($forward) . 
		"&verify=$verify");
}

/*
 * User logout via Discuz Passport API.
 * 
 * */
function _discuz_user_logout($account)
{
	global $base_url;
	
	$action		= 'logout';
	$member = array(
		'cookietime'	=> '',
		'time'				=> time(),
		'username'		=> $account->name,
		'password'		=> $account->pass,
		'email'				=> $account->mail,
		'credits'			=> '',
		'regip'				=> '',
		'regdate'			=> '',
		'msn'					=> ''
	);
	$auth				= passport_encrypt(passport_encode($member), PRIVATE_KEY);
	$forward		= $base_url;
	$verify			= md5($action . $auth . $forward . PRIVATE_KEY);

	drupal_goto($base_url . "/modules/discuz/forum/api/passport.php" . 
		"?action=$action" . 
		"&auth=" . rawurlencode($auth) . 
		"&forward=" . rawurlencode($forward) . 
		"&verify=$verify");
}

/*
 * Below several functions provided by Discuz 
 * to handle the Passport Authentication.
 * 
 * */
function passport_encrypt($txt, $key) {
	srand((double)microtime() * 1000000);
	$encrypt_key = md5(rand(0, 32000));

	$ctr = 0;
	$tmp = '';

	for($i = 0; $i < strlen($txt); $i++) {
		$ctr = $ctr == strlen($encrypt_key) ? 0 : $ctr;
		$tmp .= $encrypt_key[$ctr].($txt[$i] ^ $encrypt_key[$ctr++]);
	}

	return base64_encode(passport_key($tmp, $key));
}

function passport_decrypt($txt, $key) {
	$txt = passport_key(base64_decode($txt), $key);

	$tmp = '';

	for ($i = 0; $i < strlen($txt); $i++) {
		$tmp .= $txt[$i] ^ $txt[++$i];
	}

	return $tmp;
}

function passport_key($txt, $encrypt_key) {
	$encrypt_key = md5($encrypt_key);

	$ctr = 0;
	$tmp = '';

	for($i = 0; $i < strlen($txt); $i++) {
		$ctr = $ctr == strlen($encrypt_key) ? 0 : $ctr;
		$tmp .= $txt[$i] ^ $encrypt_key[$ctr++];
	}

	return $tmp;
}

function passport_encode($array) {
	$arrayenc = array();

	foreach($array as $key => $val) {
		$arrayenc[] = $key.'='.urlencode($val);
	}

	return implode('&', $arrayenc);
}

?>